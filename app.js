'use strict';

const config = require('./config/config.json');
const express = require('express');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./api/swagger/swagger.json');
const bodyParser = require('body-parser');
const userRoutes = require('./routing/user.routes');
const chatRoutes = require('./routing/chat.routes');
const messageRoutes = require('./routing/message.routes');
const authRoutes = require('./routing/auth.routes');
const authMiddleware = require('./middlewares/auth.middleware');

const app = express();

const userRouter = express.Router();
const chatRouter = express.Router();
const messageRouter = express.Router();
const authRouter = express.Router();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(cors());

app.use('/user', authMiddleware.getToken, userRouter);
app.use('/chat', authMiddleware.getToken, chatRouter);
app.use('/message', authMiddleware.getToken, messageRouter);
app.use('/auth', authRouter);

userRoutes.setup(userRouter);
chatRoutes.setup(chatRouter);
messageRoutes.setup(messageRouter);
authRoutes.setup(authRouter);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(config.app.port, () => {
    console.log('Server started on port: ' + config.app.port);
});