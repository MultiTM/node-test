const knex = require('knex');
const config = require('./knexfile').development;

exports.db = knex(config);

exports.TABLES =  {  
    USERS: 'users',
    CHATS: 'chats',
    USERS_CHATS: 'users_chats',
    MESSAGES: 'messages'
};