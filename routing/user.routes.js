'use strict';

const userController = require('../controllers/user.controller');

exports.setup = router => {
    router.route('/')
        .get(userController.getAll);

    router.route('/id/:id')
        .get(userController.get)
        .put(userController.update)
        .delete(userController.delete);

    router.route('/chats')
        .get(userController.getUserChats);
};