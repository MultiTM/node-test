'use strict';

const messageController = require('../controllers/message.controller');

exports.setup = router => {
    router.route('/')
        .get(messageController.getAll)
        .post(messageController.add);

    router.route('/id/:id')
        .get(messageController.get)
        .put(messageController.update)
        .delete(messageController.delete);
};