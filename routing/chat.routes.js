'use strict';

const chatController = require('../controllers/chat.controller');

exports.setup = router => {
    router.route('/')
        .get(chatController.getAll)
        .post(chatController.add);

    router.route('/id/:id')
        .get(chatController.get)
        .put(chatController.update)
        .delete(chatController.delete);

    router.route('/id/:id/join')
        .post(chatController.join);

    router.route('/id/:id/leave')
        .delete(chatController.leave);

    router.route('/id/:id/users')
        .get(chatController.getUserList);
};