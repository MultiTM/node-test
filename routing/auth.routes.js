'use strict';

const authController = require('../controllers/auth.controller');

exports.setup = router => {
    router.route('/login')
        .post(authController.login);

    router.route('/register')
        .post(authController.register);
}