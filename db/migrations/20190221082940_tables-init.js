
exports.up = function (knex, Promise) {
    return Promise.all([
        knex.schema.createTable('users', table => {
            table.increments('id').primary();
            table.string('username');
            table.string('password');
        }),
        knex.schema.createTable('chats', table => {
            table.increments('id').primary();
            table.string('title');
        }),
        knex.schema.createTable('messages', table => {
            table.increments('id').primary();
            table.string('message');
            table.integer('chat_id')
                .references('chats.id')
                .onDelete('CASCADE');
            table.integer('author_id')  
                .references('users.id')
                .onDelete('CASCADE');
        }),
        knex.schema.createTable('users_chats', table => {
            table.increments('id').primary();
            table.integer('user_id')
                .references('users.id')
                .onDelete('CASCADE');
            table.integer('chat_id')
                .references('chats.id')
                .onDelete('CASCADE');
        })
    ]);
};

exports.down = function (knex, Promise) {
    return Promise.all([
        knex.schema.dropTable('users_chats'),
        knex.schema.dropTable('messages'),
        knex.schema.dropTable('chats'),
        knex.schema.dropTable('users')
    ]);
};
