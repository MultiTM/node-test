'use strict';

const { db, TABLES } = require('../db');
const jwt = require('jsonwebtoken');
const env = require('../config/config.json');

exports.login = (req, res) => {
    const username = req.body.username;
    const password = req.body.password;

    if (username && password) {
        db.table(TABLES.USERS).select().where({ username: username, password: password }).first()
            .then((user) => {
                if (user) {
                    const token = jwt.sign({ username: user.username, id: user.id },
                        env.security.secret,
                        { expiresIn: '24h' });
                    res.json({ token });
                } else {
                    res.status(400).send('Invalid username or password');
                }
            })
            .catch(err => {
                res.status(500).send(err);
            });
    } else {
        res.status(400).send('Bad request');
    }
}

exports.register = (req, res) => {
    const user = {
        username: req.body.username,
        password: req.body.password
    };

    if (user.username && user.password) {
        db.table(TABLES.USERS).select('username').where({ username: user.username })
            .then(users => {

                if (users.length !== 0) {
                    res.status(400).send('Username is already taken');
                } else {
                    db.table(TABLES.USERS).insert(user)
                        .then(() => {
                            res.status(200).send('Ok');
                        })
                        .catch(err => {
                            res.status(500).send(err);
                        });
                }
            })
            .catch(err => {
                res.status(500).send(err);
            });
    } else {
        res.status(400).send('Bad request');
    }
}