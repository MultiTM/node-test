'use strict';

const { db, TABLES } = require('../db');

exports.get = (req, res) => {
    db.table(TABLES.CHATS).select().where({ id: req.params.id }).first()
        .then(chat => {
            if (chat) {
                res.status(200).send(chat);
            } else {
                res.status(404).send('Chat with specified id not found');
            }
        })
        .catch(err => {
            res.status(500).send(err);
        });
}

exports.getAll = (req, res) => {
    db.table(TABLES.CHATS).select()
        .then(chats => {
            res.status(200).send(chats);
        })
        .catch(err => {
            res.status(500).send(err);
        });
}

exports.add = (req, res) => {
    db.table(TABLES.CHATS).select('title').where({ title: req.body.title })
        .then(chats => {
            if (chats.length !== 0) {
                res.status(400).send('Chat with same title already exists');
            } else {
                db.table(TABLES.CHATS).insert(req.body)
                    .then(() => {
                        res.status(200).send('Ok');
                    })
                    .catch(err => {
                        res.status(500).send(err);
                    });
            }
        })
        .catch(err => {
            res.status(500).send(err);
        });
}

exports.update = (req, res) => {
    db.table(TABLES.CHATS).select().where({ id: req.params.id }).first()
        .then(chat => {

            if (!chat) {
                res.status(404).send('Chat with specified id not found');
            }

            db.table(TABLES.CHATS).update(req.body).where({ id: req.params.id })
                .then(() => {
                    res.status(200).send('Ok');
                })
                .catch(err => {
                    res.status(500).send(err);
                });
        })
        .catch(err => {
            res.status(500).send(err);
        });
}

exports.delete = (req, res) => {
    db.table(TABLES.CHATS).select().where({ id: req.params.id }).first()
        .then(chat => {

            if (!chat) {
                res.status(404).send('Chat with specified id not found');
            }

            db.table(TABLES.CHATS).del().where({ id: req.params.id })
                .then(() => {
                    res.status(200).send('Ok');
                })
                .catch(err => {
                    res.status(500).send(err);
                });
        })
        .catch(err => {
            res.status(500).send(err);
        });
}

exports.join = (req, res) => {
    const user_chat = {
        user_id: req.tokenPayload.id,
        chat_id: req.params.id
    };

    db.table(TABLES.CHATS).select().where({ id: user_chat.chat_id }).first()
        .then(chat => {

            if (chat) {
                db.table(TABLES.USERS_CHATS).insert(user_chat)
                    .then(() => {
                        res.status(200).send('Ok');
                    })
                    .catch(err => {
                        res.status(500).send(err);
                    });
            } else {
                res.status(404).send('Chat with specified id not found');
            }
        })
        .catch(err => {
            res.status(500).send(err);
        });
}

exports.leave = (req, res) => {
    db.table(TABLES.USERS_CHATS).select()
        .where({ user_id: req.tokenPayload.id, chat_id: req.params.id }).first()
        .then(user_chat => {

            if (user_chat) {
                db.table(TABLES.USERS_CHATS).del()
                    .where({ user_id: req.tokenPayload.id, chat_id: req.params.id })
                    .then(() => {
                        res.status(200).send('Ok');
                    })
                    .catch(err => {
                        res.status(500).send(err);
                    });
            } else {
                res.status(404).send('User is not in chat');
            }
        })
        .catch(err => {
            res.status(500).send(err);
        });
}

exports.getUserList = (req, res) => {
    db.table(TABLES.CHATS).select().where({ id: req.params.id }).first()
        .then(chat => {
            if (chat) {
                db.table(TABLES.USERS_CHATS)
                    .join(TABLES.USERS, 'users_chats.user_id', 'users.id')
                    .select('users.id as id', 'username')
                    .where({ chat_id: req.params.id })
                    .then(users => {
                        res.send(users);
                    })
                    .catch(err => {
                        res.status(500).send(err);
                    });
            } else {
                res.status(404).send('Chat with specified id not found');
            }
        })
        .catch(err => {
            res.status(500).send(err);
        });
}