'use strict';

const { db, TABLES } = require('../db');

exports.get = (req, res) => {
    db.table(TABLES.MESSAGES).select().where({ id: req.params.id }).first()
        .then(message => {
            if (message) {
                res.status(200).send(message);
            } else {
                res.status(404).send('Message with specified id not found');
            }
        })
        .catch(err => {
            res.status(500).send(err);
        });
}

exports.getAll = (req, res) => {
    db.table(TABLES.MESSAGES).select()
        .then(messages => {
            res.status(200).send(messages);
        })
        .catch(err => {
            res.status(500).send(err);
        });
}

exports.add = (req, res) => {
    db.table(TABLES.MESSAGES).insert(req.body)
        .then(() => {
            res.status(200).send('Ok');
        })
        .catch(err => {
            res.status(500).send(err);
        });
}

exports.update = (req, res) => {
    db.table(TABLES.MESSAGES).select().where({ id: req.params.id }).first()
        .then(message => {
            if (message) {
                db.table(TABLES.MESSAGES).update(req.body).where({ id: req.params.id })
                    .then(() => {
                        res.status(200).send('Ok');
                    })
                    .catch(err => {
                        res.status(500).send(err);
                    });
            } else {
                res.status(404).send('Message with specified id not found')
            }
        })
        .catch(err => {
            res.status(500).send(err);
        });
}

exports.delete = (req, res) => {
    db.table(TABLES.MESSAGES).select().where({ id: req.params.id }).first()
        .then(message => {
            if (message) {
                db.table(TABLES.MESSAGES).del().where({ id: req.params.id })
                    .then(() => {
                        res.status(200).send('Ok');
                    })
                    .catch(err => {
                        res.status(500).send(err);
                    });
            } else {
                res.status(404).send('Message with specified id not found');
            }
        })
        .catch(err => {
            res.status(500).send(err);
        });
}