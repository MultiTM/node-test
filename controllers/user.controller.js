'use strict';

const { db, TABLES } = require('../db');

exports.get = (req, res) => {
    db.table(TABLES.USERS).select().where({ id: req.params.id }).first()
        .then(user => {
            if (user) {
                res.status(200).send(user);
            } else {
                res.status(404).send('User with scpecified id not found');
            }
        })
        .catch(err => {
            res.status(500).send(err);
        });
}

exports.getAll = (req, res) => {
    db.table(TABLES.USERS).select()
        .then(users => {
            res.status(200).send(users);
        })
        .catch(err => {
            res.status(500).send(err);
        });
}

exports.update = (req, res) => {
    db.table(TABLES.USERS).select().where({ id: req.params.id }).first()
        .then(user => {
            if (user) {
                db.table(TABLES.USERS).update(req.body).where({ id: req.params.id })
                    .then(() => {
                        res.status(200).send('Ok');
                    })
                    .catch(err => {
                        res.status(500).send(err);
                    });
            } else {
                res.status(404).send('User with scpecified id not found');
            }
        })
        .catch(err => {
            res.status(500).send(err);
        });
}

exports.delete = (req, res) => {
    db.table(TABLES.USERS).select().where({ id: req.params.id }).first()
        .then(user => {
            if (user) {
                db.table(TABLES.USERS).del().where({ id: req.params.id })
                    .then(() => {
                        res.status(200).send('Ok');
                    })
                    .catch(err => {
                        res.status(500).send(err);
                    });
            } else {
                res.status(404).send('User with scpecified id not found');
            }
        })
        .catch(err => {
            res.status(500).send(err);
        });
}

exports.getUserChats = (req, res) => {
    db.table(TABLES.USERS).select().where({ id: req.params.id }).first()
        .then(user => {
            if (user) {
                db.table(TABLES.USERS_CHATS)
                    .join(TABLES.CHATS, 'users_chats.chat_id', 'chats.id')
                    .select('chat_id as id', 'title')
                    .where({ user_id: req.tokenPayload.id })
                    .then(chats => {
                        res.status(200).send(chats);
                    })
                    .catch(err => {
                        res.status(500).send(err);
                    });
            } else {
                res.status(404).send('User with scpecified id not found');
            }
        })
        .catch(err => {
            res.status(500).send(err);
        });
}