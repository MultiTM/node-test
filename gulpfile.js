'use strict';

const gulp = require('gulp');
const yaml = require('js-yaml');
const path = require('path');
const fs = require('fs');

gulp.task('swagger', (done) => {
    const doc = yaml.safeLoad(fs.readFileSync(path.join(__dirname, 'api/swagger/swagger.yaml')));
    fs.writeFileSync(path.join(__dirname, 'api/swagger/swagger.json'), JSON.stringify(doc, null, ' '));

    done();
});

gulp.task('watch', () => {
    gulp.watch('api/swagger/swagger.yaml', gulp.series('swagger'));
})