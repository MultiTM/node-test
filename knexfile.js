const config = require('./config/config.json');

module.exports = {
  development: {
    client: 'pg',
    connection: config.db.connection,
    migrations: {
      directory: './db/migrations'
    },
  }
}