'use strict';

const config = require('../config/config.json');
const jwt = require('jsonwebtoken');

exports.getToken = (req, res, next) => {
    let token = req.headers['x-access-token'] || req.headers['authorization'];
    if (token && token.startsWith('Bearer ')) {
        token = token.slice(7, token.length);
    }

    if (token) {
        jwt.verify(token, config.security.secret, (err, decoded) => {
            if (err){
                return res.status(401).send('Invalid token');
            } else {
                req.tokenPayload = decoded;
                next();
            }
        });
    } else {
        return res.status(401).send('Invalid token');
    }
}